
How to start prjoect:

1. Choose your development environment

2. The database used by the application is called "greenhouse" and run locally on port. 
It is necessary to manually create the database in robomongo application, and then import the collections using the file "MongoDb dump.json".

4. From terminal, run the following commands:
 - java -jar IoTGreenhouse.jar
 - node-red
 - mongo

5. On Mozilla Firefox, go to the following links:
 - localhost:1880 for node-red simulator's flow
 - import inside NodeRED the content of node-red flows.json
 - To execute the project is necessary to instal the openweather map component and set the openweather map API Key.
   There are 2 openweather map components in the subflows "5DaysForecast" e "CurrentWeather".
   The API Key is: f53c649367e1cb7a047c90e20f2c6ce5
 - localhost:1880/ui for the front end web page
 - In the subflow "SendEmail" there is a send email component, it is necessary to change the sending credentials to be able to send email.
   The address can be changed directly in the component settings.
   The destination address is retrieved from the database at runtime and can be configured in the user web page at the address
   127.0.0.1:1880/greenhouseSettings
