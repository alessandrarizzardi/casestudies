package devices;

/**
 * Class representing a light implementation of device
 * 
 * @author Filippo Pelosi
 * @version 1.0
 *
 */
public class Light extends Device<Boolean> {

	/**
	 * Create a new light with the given id and name
	 * 
	 * @param id   the id of the light
	 * @param name the name of the light
	 */
	public Light(int id, String name) {
		super(id, name);
		this.setValue(Boolean.FALSE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see devices.Device#parserAndSetValue(java.lang.String)
	 */
	@Override
	public void parserAndSetValue(String valueString) {
		this.setValue(Boolean.parseBoolean(valueString));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Light [currentValue = " + this.getValue() + "]";
	}
}
