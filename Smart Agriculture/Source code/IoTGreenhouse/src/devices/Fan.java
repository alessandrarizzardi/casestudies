package devices;

/**
 * Class representing a fan implementation of device
 * 
 * @author Filippo Pelosi
 * @version 1.0
 *
 */
public class Fan extends Device<Speed> {

	/**
	 * Create a new fan with the given id and name
	 * 
	 * @param id   the id of the fan
	 * @param name the name of the fan
	 */
	public Fan(int id, String name) {
		super(id, name);
		this.setValue(Speed.OFF);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see devices.Device#parserAndSetValue(java.lang.String)
	 */
	@Override
	public void parserAndSetValue(String value) {
		int valueInt = Integer.parseInt(value);
		for (Speed speed : Speed.values()) {
			if (speed.get() == valueInt) {
				this.setValue(speed);
				break;
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Fan [currentValue = " + this.getValue().getName() + "]";
	}

}
