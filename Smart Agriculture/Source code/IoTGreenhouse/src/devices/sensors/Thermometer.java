package devices.sensors;

import devices.Device;

/**
 * Class representing a thermometer sensor implementation of device
 * 
 * @author Filippo Pelosi
 * @version 1.0
 *
 */
public class Thermometer extends Device<Float> {

	private float minValue;
	private float maxValue;

	/**
	 * Create a new thermometer sensor with the given id and name
	 * 
	 * @param id       the id of the thermometer sensor
	 * @param name     the name of the thermometer sensor
	 * @param minValue the maximum value of the thermometer sensor
	 * @param maxValue the minimum value of the thermometer sensor
	 */
	public Thermometer(int id, String name, float minValue, float maxValue) {
		super(id, name);
		this.setValue(20.0f);
		this.minValue = minValue;
		this.maxValue = maxValue;
	}

	/**
	 * Get the minimum value
	 * 
	 * @return the minimum value
	 */
	public float getMinValue() {
		return minValue;
	}

	/**
	 * Set the minimum value
	 * 
	 * @param minValue the minimum value to set
	 */
	public void setMinValue(float minValue) {
		this.minValue = minValue;
	}

	/**
	 * Get the maximum value
	 * 
	 * @return the maximum value
	 */
	public float getMaxValue() {
		return maxValue;
	}

	/**
	 * Set the maximum value
	 * 
	 * @param maxValue the maximum value to set
	 */
	public void setMaxValue(float maxValue) {
		this.maxValue = maxValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see devices.Device#parserAndSetValue(java.lang.String)
	 */
	@Override
	public void parserAndSetValue(String value) {
		this.setValue(Float.parseFloat(value));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Thermometer [minValue = " + minValue + ", maxValue = " + maxValue + ", currentValue = "
				+ this.getValue() + "]";
	}
}
