package devices.sensors;

import devices.Device;

/**
 * Class representing a light sensor implementation of device
 * 
 * @author Filippo Pelosi
 * @version 1.0
 *
 */
public class LightSensor extends Device<Integer> {

	private int minValue;
	private int maxValue;

	/**
	 * Create a new light sensor with the given id and name
	 * 
	 * @param id       the id of the light sensor
	 * @param name     the name of the light sensor
	 * @param minValue the maximum value of the light sensor
	 * @param maxValue the minimum value of the light sensor
	 */
	public LightSensor(int id, String name, int minValue, int maxValue) {
		super(id, name);
		this.setValue(8);
		this.minValue = minValue;
		this.maxValue = maxValue;
	}

	/**
	 * Get the minimum value
	 * 
	 * @return the minimum value
	 */
	public int getMinValue() {
		return minValue;
	}

	/**
	 * Set the minimum value
	 * 
	 * @param minValue the minimum value to set
	 */
	public void setMinValue(int minValue) {
		this.minValue = minValue;
	}

	/**
	 * Get the maximum value
	 * 
	 * @return the maximum value
	 */
	public int getMaxValue() {
		return maxValue;
	}

	/**
	 * Set the maximum value
	 * 
	 * @param maxValue the maximum value to set
	 */
	public void setMaxValue(int maxValue) {
		this.maxValue = maxValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see devices.Device#parserAndSetValue(java.lang.String)
	 */
	@Override
	public void parserAndSetValue(String value) {
		int val = Integer.parseInt(value);
		if (val >= minValue && val <= maxValue) {
			this.setValue(val);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LightSensor [minValue=" + minValue + ", maxValue=" + maxValue + this.getValue() + "]";
	}

}
