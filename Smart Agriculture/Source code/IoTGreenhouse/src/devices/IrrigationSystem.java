package devices;

/**
 * Class representing a irrigation system implementation of device
 * 
 * @author Filippo Pelosi
 * @version 1.0
 *
 */
public class IrrigationSystem extends Device<Boolean> {

	/**
	 * Create a new irrigation system with the given id and name
	 * 
	 * @param id   the id of the irrigation system
	 * @param name the name of the irrigation system
	 */
	public IrrigationSystem(int id, String name) {
		super(id, name);
		this.setValue(Boolean.FALSE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see devices.Device#parserAndSetValue(java.lang.String)
	 */
	@Override
	public void parserAndSetValue(String value) {
		this.setValue(Boolean.parseBoolean(value));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Irrigation System [currentValue = " + this.getValue() + "]";
	}
}
