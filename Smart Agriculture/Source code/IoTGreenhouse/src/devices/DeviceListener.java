package devices;

/**
 * The listener interface for receiving device value changes.
 * 
 * @author Filippo Pelosi
 * @version 1.0
 *
 */
public interface DeviceListener {
	/**
	 * Notify a change in the device value
	 * 
	 * @param device the device that change value
	 */
	public void onChangeDeviceValue(Device<?> device);
}
