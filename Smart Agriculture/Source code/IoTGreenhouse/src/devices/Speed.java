package devices;

/**
 * The possible speed of the fan
 * 
 * @author filip
 * @version 1.0
 *
 */
public enum Speed {
	/**
	 * Fan off
	 */
	OFF(0),
	/**
	 * Fan level 1
	 */
	LEVEL_1(1),
	/**
	 * Fan level 2
	 */
	LEVEL_2(2),
	/**
	 * Fan level 3
	 */
	LEVEL_3(3);

	private int level;

	private Speed(int level) {
		this.level = level;
	}

	/**
	 * Return the integer representation of the speed
	 * 
	 * @return the integer representation of the speed
	 */
	public int get() {
		return level;
	}

	/**
	 * Return a string representation of the name of the speed level
	 * 
	 * @return the name of the speed level
	 */
	public String getName() {
		return this.name().toLowerCase().replace("_", " ");
	}
}
