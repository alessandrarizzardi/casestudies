package devices;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class that defines the structure of a device
 * 
 * @author Filippo Pelosi
 * @version 1.0
 *
 * @param <Type> the type of data handled by the device
 */
public abstract class Device<Type> {
	private int id;
	private String name;
	private Type value;
	List<DeviceListener> listeners;

	/**
	 * Create a new device with the given id and name
	 * 
	 * @param id   the id of the device
	 * @param name the name of the device
	 */
	public Device(int id, String name) {
		this.id = id;
		this.name = name;
		listeners = new ArrayList<>();
	}

	/**
	 * Get the device id
	 * 
	 * @return the device id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Get the device name
	 * 
	 * @return the device name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get the device value
	 * 
	 * @return the device value
	 */
	public Type getValue() {
		return value;
	}

	/**
	 * Get the device listeners
	 * 
	 * @return the device listeners
	 */
	public List<DeviceListener> getListeners() {
		return listeners;
	}

	/**
	 * Add the given listener to the device
	 * 
	 * @param listener the listener to add
	 */
	public void addListener(DeviceListener listener) {
		if (listener != null) {
			this.listeners.add(listener);
		}
	}

	/**
	 * Set the device id
	 * 
	 * @param id the device id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Set the device name
	 * 
	 * @param name the device name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Set the device value
	 * 
	 * @param value the device value
	 */
	public void setValue(Type value) {
		this.value = value;
		for (DeviceListener listener : listeners) {
			listener.onChangeDeviceValue(this);
		}
	}

	/**
	 * Returns a string representation of the device value
	 * 
	 * @return the string representation of the value
	 */
	public String getStringValue() {
		return String.valueOf(value);
	}

	/**
	 * Parse the given string value and set the device value
	 * 
	 * @param value the value to set
	 */
	public abstract void parserAndSetValue(String value);

}
