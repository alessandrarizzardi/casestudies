package devices;

/**
 * Class representing a pesticide erogator implementation of device
 * 
 * @author Filippo Pelosi
 * @version 1.0
 *
 */
public class PesticideErogator extends Device<Boolean> {

	/**
	 * Create a new pesticide erogator with the given id and name
	 * 
	 * @param id   the id of the pesticide erogator
	 * @param name the name of the pesticide erogator
	 */
	public PesticideErogator(int id, String name) {
		super(id, name);
		this.setValue(Boolean.FALSE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see devices.Device#parserAndSetValue(java.lang.String)
	 */
	@Override
	public void parserAndSetValue(String valueString) {
		this.setValue(Boolean.parseBoolean(valueString));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Irrigation System [currentValue = " + this.getValue() + "]";
	}
}
