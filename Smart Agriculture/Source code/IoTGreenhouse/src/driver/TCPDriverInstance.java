package driver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import greenhousesim.SensorNetwork;

/**
 * Implementation of a thread able to handle the communication with a TCP
 * socket.
 * 
 * @author Filippo Pelosi
 * @version 1.0
 *
 */
public class TCPDriverInstance implements Runnable {

	private Socket clientSocket;
	private SensorNetwork network;

	/**
	 * Create a new TCP driver
	 * 
	 * @param clientSocket the socket to handle
	 * @param network      the sensor network with all the devices of the system
	 */
	public TCPDriverInstance(Socket clientSocket, SensorNetwork network) {
		this.clientSocket = clientSocket;
		this.network = network;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		try {
			PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

			while (true) {
				String readLine = in.readLine();
				if (readLine != null) {
					String response = network.tryReadMessage(readLine);
					out.write(response);
					out.flush();
				}
			}
		} catch (IOException ex) {
			System.err.println("An exception occurred! Message: " + ex.getMessage());
		}
	}
}
