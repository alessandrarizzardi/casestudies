package driver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import greenhousesim.SensorNetwork;

/**
 * Implementation of the TCP server.
 * 
 * @author Filippo Pelosi
 * @version 1.0
 *
 */
public class TCPServerDriver {
	private SensorNetwork network;

	/**
	 * Create a new TCP server
	 * 
	 * @param network the sensor network with all the devices of the system
	 */
	public TCPServerDriver(SensorNetwork network) {
		this.network = network;
	}

	/**
	 * Starts the server communication
	 */
	public void start() {
		Runnable task = new Runnable() {
			@Override
			public void run() {
				ServerSocket serverSocket;
				try {
					serverSocket = new ServerSocket(8888);
					try {
						while (true) {
							Socket clientSocket = serverSocket.accept();
							try {
								TCPDriverInstance instance = new TCPDriverInstance(clientSocket, network);
								Thread thread = new Thread(instance);
								thread.start();
							} catch (IllegalThreadStateException ex) {
								System.err.println("Unable to start thread! Closed connection with: "
										+ clientSocket.getInetAddress() + " (" + ex.getMessage() + ")");
								clientSocket.close();
							} catch (Exception ex) {
								System.err.println("Exception! Closed connection with: " + clientSocket.getInetAddress()
										+ " (" + ex.getMessage() + ")");
								clientSocket.close();
							}
						}
					} finally {
						serverSocket.close();
					}
				} catch (IOException ex) {
					System.err.println("An exception occurred! Message: " + ex.getMessage());
				}
			}
		};
		Thread thread = new Thread(task);
		thread.start();
	}

}
