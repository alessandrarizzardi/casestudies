package greenhousesim;

import java.util.HashMap;

import devices.Device;

/**
 * Implementation of a sensor network with all the devices of the system
 * 
 * @author Filippo Pelosi
 * @version 1.0
 *
 */
public class SensorNetwork {

	private String name;
	private HashMap<Integer, Device<?>> map;

	/**
	 * Create a new empty sensors network
	 * 
	 * @param name the name of the sensors network
	 */
	public SensorNetwork(String name) {
		this.name = name;
		map = new HashMap<Integer, Device<?>>();
	}

	/**
	 * Get the name of the sensors network
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the name of the sensors network
	 * 
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Add the given device to the sensor network
	 * 
	 * @param device the device to add
	 */
	public void addSensor(Device<?> device) {
		map.put(device.getId(), device);
	}

	/**
	 * Parse a message received by the network, sets the received value or returns
	 * the requested value
	 * 
	 * @param readLine the message received
	 * @return the outcome of the operation
	 */
	public String tryReadMessage(String readLine) {
		String[] split = readLine.split(" ");
		if (split.length == 2 || split.length == 3) {
			String operation = split[0];
			int id = Integer.parseInt(split[1]);
			Device<?> device = map.get(id);
			if (device == null) {
				return "ERROR - Unknown sensor id " + id + ": " + readLine + "\n";
			}
			if ("GET".equals(operation)) {
				String value = device.getStringValue();
				return "OK " + id + " " + value + "\n";
			} else if ("SET".equals(operation) && split.length == 3) {
				String value = split[2];
				device.parserAndSetValue(value);
				return "OK " + id + " " + value + "\n";
			} else {
				return "ERROR - Operation not supported: " + readLine + "\n";
			}
		} else {
			return "ERROR - Unable to parse message: " + readLine + "\n";
		}
	}

}
