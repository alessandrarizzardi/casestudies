package greenhousesim;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import devices.Device;
import devices.DeviceListener;
import devices.Fan;
import devices.Light;
import devices.Speed;
import devices.sensors.HumiditySensor;
import devices.sensors.LightSensor;
import devices.sensors.Thermometer;

/**
 * This class create the greenhouse interface panel
 *
 * @author Filippo Pelosi
 * @version 1.0
 */
public class GreenhousePanel extends JPanel implements DeviceListener {

	private static final long serialVersionUID = -7335367930454064458L;
	private Thermometer airThermometer;
	private HumiditySensor airHumiditySensor;
	private LightSensor lightSensor;
	private Light light;
	private Fan fan;
	private JLabel lightStateIcon;
	private JLabel fanState;
	private URL imgSensorActive;
	private URL imgSensorDisabled;
	private JLabel lblCurrentHumidity;
	private JLabel lblCurrentTemp;
	private JLabel lblCurrentLight;

	/**
	 * Create the panel
	 * 
	 * @param airThermometr     the thermometer sensor
	 * @param airHumiditySensor the humidity sensor
	 * @param lightSensor       the light sensor
	 * @param light             the light device
	 * @param fan               the fan device
	 */
	public GreenhousePanel(Thermometer airThermometr, HumiditySensor airHumiditySensor, LightSensor lightSensor,
			Light light, Fan fan) {
		super();

		this.airThermometer = airThermometr;
		this.airHumiditySensor = airHumiditySensor;
		this.lightSensor = lightSensor;
		this.light = light;
		this.fan = fan;

		this.light.addListener(this);
		this.fan.addListener(this);

		initializePanel();
	}

	private void initializePanel() {
		imgSensorActive = getClass().getResource(ImagesPaths.PATH_IMG_ACTIVE_SENSOR);
		imgSensorDisabled = getClass().getResource(ImagesPaths.PATH_IMG_DISABLED_SENSOR);
		setMinimumSize(new Dimension(500, 425));
		setSize(new Dimension(500, 425));
		setPreferredSize(new Dimension(500, 425));
		setBorder(new TitledBorder(null, "Greenhouse", TitledBorder.LEADING, TitledBorder.TOP,
				new Font("Tahoma", Font.BOLD, 18), new Color(0, 0, 0)));
		setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBorder(null);
		add(scrollPane, BorderLayout.CENTER);

		JPanel panel = new JPanel();
		panel.setBorder(null);
		scrollPane.setViewportView(panel);
		panel.setLayout(null);

		lblCurrentLight = new JLabel(lightSensor.getStringValue());
		lblCurrentLight.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblCurrentLight.setBounds(391, 310, 27, 20);
		panel.add(lblCurrentLight);

		lblCurrentTemp = new JLabel(airThermometer.getStringValue() + "\u00B0C");
		lblCurrentTemp.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblCurrentTemp.setBounds(183, 295, 72, 20);
		panel.add(lblCurrentTemp);

		lblCurrentHumidity = new JLabel(airHumiditySensor.getStringValue() + "%");
		lblCurrentHumidity.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblCurrentHumidity.setBounds(183, 331, 99, 20);
		panel.add(lblCurrentHumidity);

		JLabel lblThermometer = new JLabel("Soil thermometer");
		lblThermometer.setBounds(10, 11, 150, 20);
		lblThermometer.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(lblThermometer);

		JLabel lblHumiditySensor = new JLabel("Humidity sensor");
		lblHumiditySensor.setBounds(170, 11, 130, 20);
		lblHumiditySensor.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(lblHumiditySensor);

		JLabel lblLightSensor = new JLabel("Light sensor");
		lblLightSensor.setBounds(323, 11, 155, 20);
		lblLightSensor.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(lblLightSensor);

		JLabel lblThermometerMaxValue = new JLabel(Float.toString(airThermometer.getMaxValue()) + "\u00B0C");
		lblThermometerMaxValue.setBounds(46, 38, 50, 20);
		lblThermometerMaxValue.setFont(new Font("Tahoma", Font.PLAIN, 12));
		panel.add(lblThermometerMaxValue);

		JLabel labelMaxHumidity = new JLabel(airHumiditySensor.getMaxValue() + "%");
		labelMaxHumidity.setBounds(206, 38, 38, 20);
		labelMaxHumidity.setFont(new Font("Tahoma", Font.PLAIN, 12));
		panel.add(labelMaxHumidity);

		int maxLightSensorSensor = lightSensor.getMaxValue();
		JLabel label = new JLabel(maxLightSensorSensor + "");
		label.setBounds(378, 38, 27, 20);
		label.setFont(new Font("Tahoma", Font.PLAIN, 12));
		panel.add(label);

		int minTemp = Math.round(airThermometer.getMinValue()) * 10;
		int maxTemp = Math.round(airThermometer.getMaxValue()) * 10;
		int currTemp = Math.round(airThermometer.getValue()) * 10;
		JSlider temperatureSlider = new JSlider(minTemp, maxTemp, currTemp);
		temperatureSlider.setBounds(50, 58, 20, 200);
		temperatureSlider.setMajorTickSpacing(5);
		temperatureSlider.setPaintTicks(true);
		temperatureSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				float currentTemp = temperatureSlider.getValue() / 10.0f;
				lblCurrentTemp.setText(currentTemp + "\u00B0C");
			}
		});
		temperatureSlider.setOrientation(SwingConstants.VERTICAL);
		temperatureSlider.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(temperatureSlider);

		int minHumidity = airHumiditySensor.getMinValue();
		int maxHumidity = airHumiditySensor.getMaxValue();
		int currHumidity = airHumiditySensor.getValue();
		JSlider humiditySlider = new JSlider(minHumidity, maxHumidity, currHumidity);
		humiditySlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				int currentHumidity = humiditySlider.getValue();
				lblCurrentHumidity.setText(currentHumidity + "%");
			}
		});
		humiditySlider.setBounds(211, 58, 20, 200);
		temperatureSlider.setMajorTickSpacing(5);
		temperatureSlider.setPaintTicks(true);
		humiditySlider.setOrientation(SwingConstants.VERTICAL);
		humiditySlider.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(humiditySlider);

		int minLight = lightSensor.getMinValue();
		int maxLight = lightSensor.getMaxValue();
		int currLight = lightSensor.getValue();
		JSlider lightSlider = new JSlider(minLight, maxLight, currLight);
		lightSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				int currentLight = lightSlider.getValue();
				lblCurrentLight.setText(currentLight + "");
			}
		});
		lightSlider.setBounds(376, 58, 20, 200);
		lightSlider.setOrientation(SwingConstants.VERTICAL);
		temperatureSlider.setMajorTickSpacing(5);
		temperatureSlider.setPaintTicks(true);
		lightSlider.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(lightSlider);

		JLabel lblThermometerMInValue = new JLabel(Float.toString(airThermometer.getMinValue()) + "\u00B0C");
		lblThermometerMInValue.setBounds(45, 259, 51, 20);
		lblThermometerMInValue.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(lblThermometerMInValue);

		JLabel lblHuidityMinValue = new JLabel(airHumiditySensor.getMinValue() + "%");
		lblHuidityMinValue.setBounds(206, 259, 38, 20);
		lblHuidityMinValue.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(lblHuidityMinValue);

		int minLightSensor = lightSensor.getMinValue();
		JLabel lblLeafWetnessMinValue = new JLabel(minLightSensor + "");
		lblLeafWetnessMinValue.setBounds(378, 259, 27, 20);
		lblLeafWetnessMinValue.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(lblLeafWetnessMinValue);

		JLabel lblLight = new JLabel("Light");
		lblLight.setBounds(283, 367, 163, 20);
		lblLight.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(lblLight);

		lightStateIcon = new JLabel("");
		lightStateIcon.setIcon(new ImageIcon(imgSensorDisabled));
		lightStateIcon.setBounds(458, 367, 20, 20);
		panel.add(lightStateIcon);

		JLabel lblFan = new JLabel("Fan");
		lblFan.setBounds(10, 367, 110, 20);
		lblFan.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(lblFan);

		fanState = new JLabel(fan.getValue().getName());
		fanState.setBounds(181, 367, 84, 20);
		fanState.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(fanState);

		JLabel lblTemperature = new JLabel("Temperature:");
		lblTemperature.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblTemperature.setBounds(10, 295, 163, 20);
		panel.add(lblTemperature);

		JLabel lblHumidity = new JLabel("Humidity:");
		lblHumidity.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblHumidity.setBounds(10, 331, 163, 20);
		panel.add(lblHumidity);

		JLabel lblLight_1 = new JLabel("Light:");
		lblLight_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblLight_1.setBounds(283, 295, 135, 20);
		panel.add(lblLight_1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see devices.DeviceListener#onChangeDeviceValue(devices.Device)
	 */
	@Override
	public void onChangeDeviceValue(Device<?> device) {
		if (device instanceof Light) {
			boolean active = light.getValue();
			URL image = active ? imgSensorActive : imgSensorDisabled;
			this.lightStateIcon.setIcon(new ImageIcon(image));
		} else if (device instanceof Fan) {
			Speed speed = fan.getValue();
			this.fanState.setText(speed.getName());
		}
	}
}
