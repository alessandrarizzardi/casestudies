package greenhousesim;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import devices.Device;
import devices.DeviceListener;
import devices.IrrigationSystem;
import devices.sensors.HumiditySensor;
import devices.sensors.Thermometer;

/**
 * This class create the vegetable garden interface panel
 *
 * @author Filippo Pelosi
 * @version 1.0
 */
public class VegetableGardenPanel extends JPanel implements DeviceListener {

	private static final long serialVersionUID = -7631607745604272902L;
	private Thermometer soilThermometer;
	private HumiditySensor soilHumiditySensor;
	private IrrigationSystem irrigationSystem;
	private JLabel irrigationSystemStateIcon;
	private URL imgSensorActive;
	private URL imgSensorDisabled;
	private JLabel lblTempCurrentValue;
	private JLabel lblHumidityCurrentValue;

	/**
	 * Create the panel
	 * 
	 * @param soilThermometr     the thermometer sensor
	 * @param soilHumiditySensor the humidity sensor
	 * @param irrigationSystem   the irrigation system device
	 */
	public VegetableGardenPanel(Thermometer soilThermometr, HumiditySensor soilHumiditySensor,
			IrrigationSystem irrigationSystem) {
		super();
		this.soilThermometer = soilThermometr;
		this.soilHumiditySensor = soilHumiditySensor;
		this.irrigationSystem = irrigationSystem;

		this.irrigationSystem.addListener(this);

		initializePanel();
	}

	private void initializePanel() {
		imgSensorActive = getClass().getResource(ImagesPaths.PATH_IMG_ACTIVE_SENSOR);
		imgSensorDisabled = getClass().getResource(ImagesPaths.PATH_IMG_DISABLED_SENSOR);

		setMinimumSize(new Dimension(370, 425));
		setSize(new Dimension(370, 425));
		setPreferredSize(new Dimension(370, 425));
		setBorder(new TitledBorder(null, "Vegetable garden", TitledBorder.LEADING, TitledBorder.TOP,
				new Font("Tahoma", Font.BOLD, 18), new Color(0, 0, 0)));
		setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBorder(null);
		add(scrollPane, BorderLayout.CENTER);

		JPanel panel = new JPanel();
		panel.setBorder(null);
		scrollPane.setViewportView(panel);
		panel.setLayout(null);

		lblTempCurrentValue = new JLabel(soilThermometer.getStringValue() + "\u00B0C");
		lblTempCurrentValue.setToolTipText("");
		lblTempCurrentValue.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblTempCurrentValue.setBounds(212, 295, 80, 20);
		panel.add(lblTempCurrentValue);

		lblHumidityCurrentValue = new JLabel(soilHumiditySensor.getValue() + "%");
		lblHumidityCurrentValue.setToolTipText("");
		lblHumidityCurrentValue.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblHumidityCurrentValue.setBounds(212, 331, 80, 20);
		panel.add(lblHumidityCurrentValue);

		JLabel lblThermometer = new JLabel("Soil thermometer");
		lblThermometer.setBounds(10, 11, 140, 20);
		lblThermometer.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(lblThermometer);

		JLabel lblHumiditySensor = new JLabel("Humidity sensor");
		lblHumiditySensor.setBounds(200, 11, 148, 20);
		lblHumiditySensor.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(lblHumiditySensor);

		String maxTempStr = Float.toString(soilThermometer.getMaxValue());
		JLabel lblThermometerMaxValue = new JLabel(maxTempStr + "\u00B0C");
		lblThermometerMaxValue.setBounds(40, 38, 49, 20);
		lblThermometerMaxValue.setFont(new Font("Tahoma", Font.PLAIN, 12));
		panel.add(lblThermometerMaxValue);

		JLabel labelMaxHumidity = new JLabel(soilHumiditySensor.getMaxValue() + "%");
		labelMaxHumidity.setBounds(218, 38, 37, 20);
		labelMaxHumidity.setFont(new Font("Tahoma", Font.PLAIN, 12));
		panel.add(labelMaxHumidity);

		int minTemp = Math.round(soilThermometer.getMinValue()) * 10;
		int maxTemp = Math.round(soilThermometer.getMaxValue()) * 10;
		int currTemp = Math.round(soilThermometer.getValue()) * 10;
		JSlider temperatureSlider = new JSlider(minTemp, maxTemp, currTemp);
		temperatureSlider.setBounds(50, 58, 20, 200);
		temperatureSlider.setMajorTickSpacing(5);
		temperatureSlider.setPaintTicks(true);
		temperatureSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				float currentTemp = temperatureSlider.getValue() / 10.0f;
				lblTempCurrentValue.setText(currentTemp + "\u00B0C");
			}
		});
		temperatureSlider.setOrientation(SwingConstants.VERTICAL);
		temperatureSlider.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(temperatureSlider);

		int minHumidity = soilHumiditySensor.getMinValue();
		int maxHumidity = soilHumiditySensor.getMaxValue();
		int currHumidity = soilHumiditySensor.getValue();
		JSlider humiditySlider = new JSlider(minHumidity, maxHumidity, currHumidity);
		humiditySlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				int currentHumidity = humiditySlider.getValue();
				lblHumidityCurrentValue.setText(currentHumidity + "%");
			}
		});
		humiditySlider.setBounds(222, 58, 20, 200);
		temperatureSlider.setMajorTickSpacing(5);
		temperatureSlider.setPaintTicks(true);
		humiditySlider.setOrientation(SwingConstants.VERTICAL);
		humiditySlider.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(humiditySlider);

		String minTempStr = Float.toString(soilThermometer.getMinValue());
		JLabel lblThermometerMInValue = new JLabel(minTempStr + "\u00B0C");
		lblThermometerMInValue.setBounds(35, 259, 50, 20);
		lblThermometerMInValue.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(lblThermometerMInValue);

		JLabel lblHuidityMinValue = new JLabel(soilHumiditySensor.getMinValue() + "%");
		lblHuidityMinValue.setBounds(222, 259, 34, 20);
		lblHuidityMinValue.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(lblHuidityMinValue);

		JLabel lblIrrigationSystem = new JLabel("Irrigation system");
		lblIrrigationSystem.setBounds(10, 367, 148, 20);
		lblIrrigationSystem.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(lblIrrigationSystem);

		irrigationSystemStateIcon = new JLabel("");
		irrigationSystemStateIcon.setIcon(new ImageIcon(imgSensorDisabled));
		irrigationSystemStateIcon.setBounds(232, 365, 20, 20);
		panel.add(irrigationSystemStateIcon);

		JLabel lblTempValue = new JLabel("Temperature:");
		lblTempValue.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblTempValue.setBounds(10, 295, 140, 20);
		panel.add(lblTempValue);

		JLabel lblHumidity = new JLabel("Humidity:");
		lblHumidity.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblHumidity.setBounds(10, 331, 140, 20);
		panel.add(lblHumidity);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see devices.DeviceListener#onChangeDeviceValue(devices.Device)
	 */
	@Override
	public void onChangeDeviceValue(Device<?> device) {
		if (device instanceof IrrigationSystem) {
			boolean active = this.irrigationSystem.getValue();
			URL image = active ? imgSensorActive : imgSensorDisabled;
			this.irrigationSystemStateIcon.setIcon(new ImageIcon(image));
		}
	}

}
