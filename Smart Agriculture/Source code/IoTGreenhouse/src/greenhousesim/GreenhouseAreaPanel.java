package greenhousesim;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import devices.Device;
import devices.DeviceListener;
import devices.IrrigationSystem;
import devices.PesticideErogator;
import devices.sensors.HumiditySensor;
import devices.sensors.LeafWetnessSensor;
import devices.sensors.Thermometer;

/**
 * This class create the greenhouse area interface panel
 *
 * @author Filippo Pelosi
 * @version 1.0
 */
public class GreenhouseAreaPanel extends JPanel implements DeviceListener {

	private static final long serialVersionUID = 6961600031061783739L;
	private int area;
	private Thermometer soilThermometer;
	private HumiditySensor soilHumiditySensor;
	private LeafWetnessSensor leafWetnessSensor;
	private PesticideErogator pesticideErogator;
	private IrrigationSystem irrigationSystem;
	private JLabel irrigationSystemStateIcon;
	private JLabel pesticideErogatorStateIcon;
	private URL imgSensorActive;
	private URL imgSensorDisabled;
	private JLabel lblCurrentTemp;
	private JLabel lblCurrentHumidity;
	private JLabel lblCurrentLeafWetness;

	/**
	 * Create the panel
	 * 
	 * @param area               the area of the greenhouse
	 * @param soilThermometr     the thermometer sensor
	 * @param soilHumiditySensor the humidity sensor
	 * @param leafWetnessSensor  the leaf wetness sensor
	 * @param pesticideErogator  the irrigation system device
	 * @param irrigationSystem   the irrigation system device
	 */
	public GreenhouseAreaPanel(int area, Thermometer soilThermometr, HumiditySensor soilHumiditySensor,
			LeafWetnessSensor leafWetnessSensor, PesticideErogator pesticideErogator,
			IrrigationSystem irrigationSystem) {
		super();
		this.area = area;
		this.soilThermometer = soilThermometr;
		this.soilHumiditySensor = soilHumiditySensor;
		this.leafWetnessSensor = leafWetnessSensor;
		this.pesticideErogator = pesticideErogator;
		this.irrigationSystem = irrigationSystem;

		this.pesticideErogator.addListener(this);
		this.irrigationSystem.addListener(this);

		initializePanel();
	}

	private void initializePanel() {
		imgSensorActive = getClass().getResource(ImagesPaths.PATH_IMG_ACTIVE_SENSOR);
		imgSensorDisabled = getClass().getResource(ImagesPaths.PATH_IMG_DISABLED_SENSOR);
		setMinimumSize(new Dimension(500, 425));
		setSize(new Dimension(500, 425));
		setPreferredSize(new Dimension(500, 425));
		setBorder(new TitledBorder(null, "Area " + area, TitledBorder.LEADING, TitledBorder.TOP,
				new Font("Tahoma", Font.BOLD, 18), new Color(0, 0, 0)));
		setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBorder(null);
		add(scrollPane, BorderLayout.CENTER);

		JPanel panel = new JPanel();
		panel.setBorder(null);
		scrollPane.setViewportView(panel);
		panel.setLayout(null);

		lblCurrentTemp = new JLabel(soilThermometer.getStringValue() + "\u00B0C");
		lblCurrentTemp.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblCurrentTemp.setBounds(183, 295, 72, 20);
		panel.add(lblCurrentTemp);

		lblCurrentHumidity = new JLabel(soilHumiditySensor.getValue() + "%");
		lblCurrentHumidity.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblCurrentHumidity.setBounds(183, 331, 99, 20);
		panel.add(lblCurrentHumidity);

		lblCurrentLeafWetness = new JLabel(leafWetnessSensor.getValue() + "");
		lblCurrentLeafWetness.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblCurrentLeafWetness.setBounds(414, 295, 64, 20);
		panel.add(lblCurrentLeafWetness);

		JLabel lblThermometer = new JLabel("Soil thermometer");
		lblThermometer.setBounds(10, 11, 150, 20);
		lblThermometer.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(lblThermometer);

		JLabel lblHumiditySensor = new JLabel("Humidity sensor");
		lblHumiditySensor.setBounds(170, 11, 130, 20);
		lblHumiditySensor.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(lblHumiditySensor);

		JLabel lblLeafWetnessSensor = new JLabel("Leaf wetness sensor");
		lblLeafWetnessSensor.setBounds(323, 11, 155, 20);
		lblLeafWetnessSensor.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(lblLeafWetnessSensor);

		JLabel lblThermometerMaxValue = new JLabel(Float.toString(soilThermometer.getMaxValue()) + "\u00B0C");
		lblThermometerMaxValue.setBounds(46, 38, 50, 20);
		lblThermometerMaxValue.setFont(new Font("Tahoma", Font.PLAIN, 12));
		panel.add(lblThermometerMaxValue);

		JLabel labelMaxHumidity = new JLabel(soilHumiditySensor.getMaxValue() + "%");
		labelMaxHumidity.setBounds(206, 38, 38, 20);
		labelMaxHumidity.setFont(new Font("Tahoma", Font.PLAIN, 12));
		panel.add(labelMaxHumidity);

		JLabel label = new JLabel(leafWetnessSensor.getMaxValue() + "");
		label.setBounds(378, 38, 27, 20);
		label.setFont(new Font("Tahoma", Font.PLAIN, 12));
		panel.add(label);

		int minTemp = Math.round(soilThermometer.getMinValue()) * 10;
		int maxTemp = Math.round(soilThermometer.getMaxValue()) * 10;
		int currTemp = Math.round(soilThermometer.getValue()) * 10;
		JSlider temperatureSlider = new JSlider(minTemp, maxTemp, currTemp);
		temperatureSlider.setBounds(50, 58, 20, 200);
		temperatureSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				float currentTemp = temperatureSlider.getValue() / 10.0f;
				lblCurrentTemp.setText(currentTemp + "\u00B0C");
			}
		});
		temperatureSlider.setOrientation(SwingConstants.VERTICAL);
		temperatureSlider.setMajorTickSpacing(5);
		temperatureSlider.setPaintTicks(true);
		temperatureSlider.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(temperatureSlider);

		int minHumidity = soilHumiditySensor.getMinValue();
		int maxHumidity = soilHumiditySensor.getMaxValue();
		int currHumidity = soilHumiditySensor.getValue();
		JSlider humiditySlider = new JSlider(minHumidity, maxHumidity, currHumidity);
		humiditySlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				int currentHumidity = humiditySlider.getValue();
				lblCurrentHumidity.setText(currentHumidity + "%");
			}
		});
		humiditySlider.setBounds(211, 58, 20, 200);
		humiditySlider.setOrientation(SwingConstants.VERTICAL);
		temperatureSlider.setMajorTickSpacing(5);
		temperatureSlider.setPaintTicks(true);
		humiditySlider.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(humiditySlider);

		int minLeafWetness = leafWetnessSensor.getMinValue();
		int maxLeafWetness = leafWetnessSensor.getMaxValue();
		int currLeafWetness = leafWetnessSensor.getValue();
		JSlider leafWetnessSlider = new JSlider(minLeafWetness, maxLeafWetness, currLeafWetness);
		leafWetnessSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				int currentLeafWetness = leafWetnessSlider.getValue();
				lblCurrentLeafWetness.setText(currentLeafWetness + "");
			}
		});
		leafWetnessSlider.setBounds(376, 58, 20, 200);
		leafWetnessSlider.setOrientation(SwingConstants.VERTICAL);
		temperatureSlider.setMajorTickSpacing(5);
		temperatureSlider.setPaintTicks(true);
		leafWetnessSlider.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(leafWetnessSlider);

		JLabel lblThermometerMInValue = new JLabel(Float.toString(soilThermometer.getMinValue()) + "\u00B0C");
		lblThermometerMInValue.setBounds(45, 259, 51, 20);
		lblThermometerMInValue.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(lblThermometerMInValue);

		JLabel lblHuidityMinValue = new JLabel(soilHumiditySensor.getMinValue() + "%");
		lblHuidityMinValue.setBounds(206, 259, 38, 20);
		lblHuidityMinValue.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(lblHuidityMinValue);

		int minLeafWetnessSensor = leafWetnessSensor.getMinValue();
		JLabel lblLeafWetnessMinValue = new JLabel(minLeafWetnessSensor + "");
		lblLeafWetnessMinValue.setBounds(378, 259, 27, 20);
		lblLeafWetnessMinValue.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(lblLeafWetnessMinValue);

		JLabel lblIrrigationSystem = new JLabel("Irrigation system");
		lblIrrigationSystem.setBounds(10, 367, 163, 20);
		lblIrrigationSystem.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(lblIrrigationSystem);

		irrigationSystemStateIcon = new JLabel("");
		irrigationSystemStateIcon.setIcon(new ImageIcon(imgSensorDisabled));
		irrigationSystemStateIcon.setBounds(224, 367, 20, 20);
		panel.add(irrigationSystemStateIcon);

		JLabel lblPesticideErogator = new JLabel("Pesticide erogator");
		lblPesticideErogator.setBounds(283, 367, 163, 20);
		lblPesticideErogator.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(lblPesticideErogator);

		pesticideErogatorStateIcon = new JLabel("");
		pesticideErogatorStateIcon.setIcon(new ImageIcon(imgSensorDisabled));
		pesticideErogatorStateIcon.setBounds(458, 367, 20, 20);
		panel.add(pesticideErogatorStateIcon);

		JLabel label_1 = new JLabel("Temperature:");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		label_1.setBounds(10, 295, 163, 20);
		panel.add(label_1);

		JLabel label_2 = new JLabel("Humidity:");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 13));
		label_2.setBounds(10, 331, 163, 20);
		panel.add(label_2);

		JLabel lblLeafWetness = new JLabel("Leaf wetness:");
		lblLeafWetness.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblLeafWetness.setBounds(283, 295, 135, 20);
		panel.add(lblLeafWetness);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see devices.DeviceListener#onChangeDeviceValue(devices.Device)
	 */
	@Override
	public void onChangeDeviceValue(Device<?> device) {
		if (device instanceof IrrigationSystem) {
			boolean active = irrigationSystem.getValue();
			URL image = active ? imgSensorActive : imgSensorDisabled;
			this.irrigationSystemStateIcon.setIcon(new ImageIcon(image));
		} else if (device instanceof PesticideErogator) {
			boolean active = pesticideErogator.getValue();
			URL image = active ? imgSensorActive : imgSensorDisabled;
			this.pesticideErogatorStateIcon.setIcon(new ImageIcon(image));
		}
	}
}
