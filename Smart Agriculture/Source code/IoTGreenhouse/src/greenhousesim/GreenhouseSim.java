package greenhousesim;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Toolkit;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import devices.Fan;
import devices.IrrigationSystem;
import devices.Light;
import devices.PesticideErogator;
import devices.sensors.HumiditySensor;
import devices.sensors.LeafWetnessSensor;
import devices.sensors.LightSensor;
import devices.sensors.Thermometer;
import driver.TCPServerDriver;

/**
 * This class create the main librarian frame
 * 
 * @author Filippo Pelosi
 * @version 1.0
 *
 */
public class GreenhouseSim extends JFrame {

	private static final long serialVersionUID = -3283910890902411686L;
	private JPanel contentPane;

	/**
	 * Launch the application
	 * @param args the args input
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SensorNetwork network = new SensorNetwork("Greenhouse");
					GreenhouseSim frame = new GreenhouseSim(network);
					frame.setVisible(true);
					TCPServerDriver tp = new TCPServerDriver(network);
					tp.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	private GreenhouseSim(SensorNetwork network) {
		Thermometer greenhouseAirThermometer = new Thermometer(1, "Greenhouse air thermometer", -10, 40);
		network.addSensor(greenhouseAirThermometer);
		HumiditySensor greenhouseAirHumiditySensor = new HumiditySensor(2, "Greenhouse air humidity sensor", 0, 100);
		network.addSensor(greenhouseAirHumiditySensor);
		LightSensor lightSensor = new LightSensor(3, "Light sensor", 0, 10);
		network.addSensor(lightSensor);
		Fan fan = new Fan(16, "Greenhouse fan");
		network.addSensor(fan);
		Light light = new Light(17, "Greenhouse light");
		network.addSensor(light);

		Thermometer greenhouseSoilThermometerArea1 = new Thermometer(4, "Greenhouse soil thermometer area 1", -10, 40);
		network.addSensor(greenhouseSoilThermometerArea1);
		HumiditySensor greenhouseSoilHumiditySensorArea1 = new HumiditySensor(5,
				"Greenhouse soil humidity sensor area 1", 0, 100);
		network.addSensor(greenhouseSoilHumiditySensorArea1);
		LeafWetnessSensor leafWetnessSensorArea1 = new LeafWetnessSensor(12, "Leaf wetness sensor area 1", 0, 15);
		network.addSensor(leafWetnessSensorArea1);
		PesticideErogator pesticideErogatorArea1 = new PesticideErogator(18, "Pesticide erogator area 1");
		network.addSensor(pesticideErogatorArea1);
		IrrigationSystem irrigationSystemArea1 = new IrrigationSystem(22, "Irrigation system area 1");
		network.addSensor(irrigationSystemArea1);

		Thermometer greenhouseSoilThermometerArea2 = new Thermometer(6, "Greenhouse soil thermometer area 2", -10, 40);
		network.addSensor(greenhouseSoilThermometerArea2);
		HumiditySensor greenhouseSoilHumiditySensorArea2 = new HumiditySensor(7,
				"Greenhouse soil humidity sensor area 2", 0, 100);
		network.addSensor(greenhouseSoilHumiditySensorArea2);
		LeafWetnessSensor leafWetnessSensorArea2 = new LeafWetnessSensor(13, "Leaf wetness sensor area 2", 0, 15);
		network.addSensor(leafWetnessSensorArea2);
		PesticideErogator pesticideErogatorArea2 = new PesticideErogator(19, "Pesticide erogator area 2");
		network.addSensor(pesticideErogatorArea2);
		IrrigationSystem irrigationSystemArea2 = new IrrigationSystem(23, "Irrigation system area 2");
		network.addSensor(irrigationSystemArea2);

		Thermometer greenhouseSoilThermometerArea3 = new Thermometer(8, "Greenhouse soil thermometer area 3", -10, 40);
		network.addSensor(greenhouseSoilThermometerArea3);
		HumiditySensor greenhouseSoilHumiditySensorArea3 = new HumiditySensor(9,
				"Greenhouse soil humidity sensor area 3", 0, 100);
		network.addSensor(greenhouseSoilHumiditySensorArea3);
		LeafWetnessSensor leafWetnessSensorArea3 = new LeafWetnessSensor(14, "Leaf wetness sensor area 3", 0, 15);
		network.addSensor(leafWetnessSensorArea3);
		PesticideErogator pesticideErogatorArea3 = new PesticideErogator(20, "Pesticide erogator area 3");
		network.addSensor(pesticideErogatorArea3);
		IrrigationSystem irrigationSystemArea3 = new IrrigationSystem(24, "Irrigation system area 3");
		network.addSensor(irrigationSystemArea3);

		Thermometer greenhouseSoilThermometerArea4 = new Thermometer(10, "Greenhouse soil thermometer area 4", -10, 40);
		network.addSensor(greenhouseSoilThermometerArea4);
		HumiditySensor greenhouseSoilHumiditySensorArea4 = new HumiditySensor(11,
				"Greenhouse soil humidity sensor area 4", 0, 100);
		network.addSensor(greenhouseSoilHumiditySensorArea4);
		LeafWetnessSensor leafWetnessSensorArea4 = new LeafWetnessSensor(15, "Leaf wetness sensor area 4", 0, 15);
		network.addSensor(leafWetnessSensorArea4);
		PesticideErogator pesticideErogatorArea4 = new PesticideErogator(21, "Pesticide erogator area 4");
		network.addSensor(pesticideErogatorArea4);
		IrrigationSystem irrigationSystemArea4 = new IrrigationSystem(25, "Irrigation system area 4");
		network.addSensor(irrigationSystemArea4);

		Thermometer vegetableGardenSoilThermometer = new Thermometer(26, "Vegetable garden soil thermometer", -10, 40);
		network.addSensor(vegetableGardenSoilThermometer);
		HumiditySensor vegetableGardenSoilHumiditySensor = new HumiditySensor(27,
				"Vegetable garden soil humidity sensor", 0, 100);
		network.addSensor(vegetableGardenSoilHumiditySensor);
		IrrigationSystem vegetableGardenIrrigationSystem = new IrrigationSystem(28,
				"Vegetable garden irrigation system");
		network.addSensor(vegetableGardenIrrigationSystem);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setExtendedState(Frame.MAXIMIZED_BOTH);
		setForeground(Color.WHITE);
		URL imgWindowIcon = getClass().getResource(ImagesPaths.PATH_IMG_MINI_LOGO);
		setIconImage(new ImageIcon(imgWindowIcon).getImage());
		setTitle("Greenhouse simulator");
		contentPane = new JPanel();
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBorder(null);
		contentPane.add(scrollPane, BorderLayout.CENTER);

		JPanel mainPanel = new JPanel();
		mainPanel.setPreferredSize(new Dimension(1545, 900));
		mainPanel.setBorder(null);
		mainPanel.setLayout(null);

		GreenhousePanel greenhousePanel = new GreenhousePanel(greenhouseAirThermometer, greenhouseAirHumiditySensor,
				lightSensor, light, fan);
		greenhousePanel.setBounds(10, 0, 500, 425);
		mainPanel.add(greenhousePanel);

		VegetableGardenPanel vegetableGardenPanel = new VegetableGardenPanel(vegetableGardenSoilThermometer,
				vegetableGardenSoilHumiditySensor, vegetableGardenIrrigationSystem);
		vegetableGardenPanel.setBounds(75, 435, 370, 425);
		mainPanel.add(vegetableGardenPanel);

		GreenhouseAreaPanel greenhouseArea1Panel = new GreenhouseAreaPanel(1, greenhouseSoilThermometerArea1,
				greenhouseSoilHumiditySensorArea1, leafWetnessSensorArea1, pesticideErogatorArea1,
				irrigationSystemArea1);
		greenhouseArea1Panel.setBounds(520, 0, 500, 425);
		mainPanel.add(greenhouseArea1Panel);

		GreenhouseAreaPanel greenhouseArea2Panel = new GreenhouseAreaPanel(2, greenhouseSoilThermometerArea2,
				greenhouseSoilHumiditySensorArea2, leafWetnessSensorArea2, pesticideErogatorArea2,
				irrigationSystemArea2);
		greenhouseArea2Panel.setBounds(1030, 0, 500, 425);
		mainPanel.add(greenhouseArea2Panel);

		GreenhouseAreaPanel greenhouseArea3Panel = new GreenhouseAreaPanel(3, greenhouseSoilThermometerArea3,
				greenhouseSoilHumiditySensorArea3, leafWetnessSensorArea3, pesticideErogatorArea3,
				irrigationSystemArea3);
		greenhouseArea3Panel.setBounds(520, 435, 500, 425);
		mainPanel.add(greenhouseArea3Panel);

		GreenhouseAreaPanel greenhouseArea4Panel = new GreenhouseAreaPanel(4, greenhouseSoilThermometerArea4,
				greenhouseSoilHumiditySensorArea4, leafWetnessSensorArea4, pesticideErogatorArea4,
				irrigationSystemArea4);
		greenhouseArea4Panel.setBounds(1030, 435, 500, 425);
		mainPanel.add(greenhouseArea4Panel);
		scrollPane.setViewportView(mainPanel);
	}

}
