package greenhousesim;

/**
 * This class contains strings representing paths of the images used in the
 * application
 * 
 * @author Filippo Pelosi
 * @version 1.0
 *
 */
public class ImagesPaths {
	/**
	 * Mini logo image path
	 */
	public static final String PATH_IMG_MINI_LOGO = "/images/logo.png";
	/**
	 * Active sensor image path
	 */
	public static final String PATH_IMG_ACTIVE_SENSOR = "/images/sensor state active.png";
	/**
	 * Disabled sensor image path
	 */
	public static final String PATH_IMG_DISABLED_SENSOR = "/images/sensor state disabled.png";

}
