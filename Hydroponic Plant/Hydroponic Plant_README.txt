
How to start the project:

1. Run VM with VirtualBox application (VM password: iot)

2. From terminal, run the following commands:
 - node-red
 - mongo

3. On Mozilla Firefox, go to the following links:
 - localhost:1880 for node-red simulator's flow
 - localhost:1880/plantation for the front end web page