
package sensors;

import java.util.Date;

public abstract class IoDevice<typeOfValue> {
    //fields
    private int IDX;
    private String name;
    private typeOfValue value;
    private IODeviceListener listener;
    private Date switchOnTime; 
    private Date switchOffTime; 
    private float powerConsumption = 0;
    private float powerConsumptionForHour = 0;
    
    //constructors
    public IoDevice(String name) {
        this.name = name;
    }
    
    //methods

    public int getIDX() {
        return IDX;
    }

    public String getName() {
        return name;
    }
    
    public IODeviceListener getListener() {
        return listener;
    }

    public typeOfValue getValue() {
        return value;
    }

    public void setIDX(int IDX) {
        this.IDX = IDX;
        CommitListener();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setListener(IODeviceListener listener) {
        this.listener = listener;
    }

    public void setValue(typeOfValue value) {
        this.value = value;
        CommitListener();
    }
    
    public void CommitListener() {
        if(listener != null) listener.onChangeDeviceValue();
    }
    
    public String getValueString() {
        return String.valueOf(value);
    }

    public Date getSwitchOnTime() {
        return switchOnTime;
    }

    public void setSwitchOnTime(Date switchOnTime) {
        this.switchOnTime = switchOnTime;
    }

    public Date getSwitchOffTime() {
        return switchOffTime;
    }

    public void setSwitchOffTime(Date switchOffTime) {
        this.switchOffTime = switchOffTime;
    }

    public float getPowerConsumption() {
        return powerConsumption;
    }

    public void setPowerConsumption(float powerConsumption) {
        this.powerConsumption = powerConsumption;
    }

    public float getPowerConsumptionForHour() {
        return powerConsumptionForHour;
    }

    public void setPowerConsumptionForHour(float powerConsumptionForHour) {
        this.powerConsumptionForHour = powerConsumptionForHour;
    }
    
    public abstract void parserAndSetValue(String valueString);

    @Override
    public String toString() {
        return "{" + "IDX: " + IDX + 
                ", name: \"" + name + 
                "\", value: \"" + value +
                "\", powerConsumption: " + powerConsumption + "}";
    }
    
    
}
