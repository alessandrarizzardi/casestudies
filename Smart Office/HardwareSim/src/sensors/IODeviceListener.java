package sensors;

public interface IODeviceListener {
    
    public void onChangeDeviceValue();
    
}
