
package sensors;

public class AnalogDevice extends IoDevice<Float> {
    
    public AnalogDevice(String name){
        super(name);
        this.setValue(new Float(0.0));
    }
    
    @Override
    public void parserAndSetValue(String valueString) {
        float parseFloat = Float.parseFloat(valueString);
        this.setValue(parseFloat);
    }
}
