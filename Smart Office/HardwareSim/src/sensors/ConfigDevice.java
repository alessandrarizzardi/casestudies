package sensors;


public class ConfigDevice extends IoDevice<Integer> {
    
    public ConfigDevice(String name){
        super(name);
        this.setValue(0);
    }
    
    @Override
    public void parserAndSetValue(String valueString) {
        int parseInt = Integer.parseInt(valueString);
        this.setValue(parseInt);
    }
    
}
