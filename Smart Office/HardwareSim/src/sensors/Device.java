
package sensors;

import java.io.PrintWriter;
import java.util.HashMap;

public class Device {
    
    private int idx = 0;
    private String idDevice;
    private HashMap<Integer, IoDevice> map;
    
    public Device(String idDevice) {
        this.idDevice = idDevice;
        map = new HashMap<Integer, IoDevice>();
    }
    
    public void addDevice(IoDevice iodevice){
        idx++;
        map.put(idx, iodevice);
        iodevice.setIDX(idx);
    }
    
    public void interpret(String readLine, PrintWriter out){
        String[] split = readLine.split(" ");
        
        if("GET".equals(split[0])) {
            String idx_string = split[1];
            int parseInt = Integer.parseInt(idx_string);
            IoDevice get = map.get(parseInt);
            if(get != null) {
                String value = get.getValueString();
                out.write("OK " + idx_string + " " + value + "\n");
            }
        } else if("SET".equals(split[0])) {
            String idx_string = split[1];
            int parseInt = Integer.parseInt(idx_string);
            IoDevice get = map.get(parseInt);
            System.out.println("OK " + idx_string);
            if(get != null) {
                String value = split[2];
                get.parserAndSetValue(value);
                out.write("OK " + idx_string + " " + value + "\n");
            }
        } else if("GETALL".equals(split[0])) {
            String deviceList = "";
            int size = map.values().size();
            int count = 0;
            for (IoDevice device : map.values()) {
                count++;
                System.out.println("ALL " + device.toString());
                deviceList += size == count ? device.toString() : device.toString() + ", ";
                // reset power consumption after taking the value
                // in order to not save it more than once
                device.setPowerConsumption(0); 
            }
            out.write("[" + deviceList + "]");
        } else if("GETALLACTIVE".equals(split[0])) {
            String deviceList = "";
            for (IoDevice device : map.values()) {
                if(device.getValueString().equals("true")) {
                    System.out.println("GETALLACTIVE " + device.toString());
                    deviceList += device.toString() + ", ";
                }
            }
            out.write("[" + deviceList + "]");
        } else if("GETPOWERCONSUMPTION".equals(split[0])) {
            String deviceList = "";
            int size = map.values().size();
            int count = 0;
            for (IoDevice device : map.values()) {
                count++;
                if (device.getPowerConsumption() != 0) {
                    System.out.println("GETPOWERCONSUMPTION " + device.toString());
                    deviceList += size == count ? device.toString() : device.toString() + ", ";
                    // reset power consumption after taking the value
                    // in order to not save it more than once
                    device.setPowerConsumption(0);
                } 
            }
            out.write("[" + deviceList + "]");
        } else {
            out.write("ERROR 1 " + readLine + "\n"); //ERROR 1 unknown command
        }
        
    }
}
