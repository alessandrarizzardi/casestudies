package driver;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import sensors.Device;

public class TCPDriverinstance implements Runnable{
    
    private Socket clientSocket;
    private Device console;
    
    public TCPDriverinstance(Socket clientSocket, Device console) {
        this.clientSocket = clientSocket;
        this.console = console;
    }
    
    @Override
    public void run() {
        try {
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            
            while(true) {
                String readLine = in.readLine();
                if (readLine != null) {
                    console.interpret(readLine, out);
                    out.flush();
                }
            }
        } catch(Exception ex) {
            System.out.println(ex);
        }
    }
}
