package driver;

import java.net.ServerSocket;
import java.net.Socket;
import sensors.Device;

public class TCPServerDriver {
    
    Device device;
    
    public TCPServerDriver(Device dev) {
        device = dev;
    }
    
    public void Start() {
        try {
            ServerSocket serverSocket = new ServerSocket(9090);
            while(true) {
                Socket clientSocket = serverSocket.accept();
                TCPDriverinstance instance = new TCPDriverinstance(clientSocket, device);
                Thread thread = new Thread(instance);
                thread.start();
            }
        } catch(Exception ex) {
            System.out.println(ex);
        }
    }
}
