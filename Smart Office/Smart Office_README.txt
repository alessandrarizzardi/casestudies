
How to start prjoect:

1. Choose your development environment

2. The dump of the database is available in MongoDump folder

3. Start the java project contained in HardwareSim folder

4. From terminal, run the following commands:
 - node-red
 - mongo

5. On Mozilla Firefox, go to the following links:
 - localhost:1880 for node-red simulator's flow
 - import inside NodeRED the content of noderedFlows.json
 - localhost:1880/ui for the front end web page